const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",]



function makeLayrint(mapping) {
    window.main = document.createElement('div')
    main.id = 'main'
    const fishes = document.createElement('div')
    fishes.id = 'fishes'
    main.appendChild(fishes)
    const Cmaze = document.createElement('div')
    Cmaze.id = 'maze'
    main.appendChild(Cmaze)
    document.body.appendChild(main)

    const maze = document.querySelector("#maze")
    let index = 1


    for (let line = 0; line < mapping.length; line++) {
        let lineDiv = document.createElement('div')
        lineDiv.className = 'line'
        lineDiv.id = `line${line}`
        
        for (cell of mapping[line]) {
            let div = document.createElement('div')
            if (cell == 'S' || cell == 'F') {
                div.className = `${cell} space`
                div.id = index
            } else if (cell == ' ') {
                div.className = 'space'
                div.id = index
            } else {
                div.className = 'block'
                div.id = index
            }
            lineDiv.appendChild(div) 
            index++ 
        }
        maze.appendChild(lineDiv)
    }
}


function makePlayer(selected) {
    console.log('makePlayer', selected)
    let start = document.querySelector('.S')
    let player = document.createElement('div')
    player.className = 'player'
    player.style.backgroundImage = `url('./players/${selected}.gif')`
    start.appendChild(player)
}


function movePlayer(event) {
    const tamLinha = document.querySelector('.line').childElementCount   
    let player = document.querySelector('.player')
    let cellAtual = Number(player.parentElement.id)
    const keyName = event.key
    console.log(keyName)
    if (keyName == 'ArrowUp' | keyName == 'w') {
        let cellAcima = document.querySelector(`div[id="${cellAtual - tamLinha}"]`)
        if (cellAcima.className !== 'block') {
            cellAcima.appendChild(player)
            moveSong.playAudio()
        }
    }
    if (keyName == 'ArrowDown' | keyName == 's') {
        let cellAbaixo = document.querySelector(`div[id="${cellAtual + tamLinha}"]`)
        if (cellAbaixo.className !== 'block') {
            cellAbaixo.appendChild(player)
            moveSong.playAudio()
        }
    }
    if (keyName == 'ArrowRight' | keyName == 'd') {
        let cellDireita = document.querySelector(`div[id="${cellAtual + 1}"]`)
        if (cellDireita.className !== 'block' && cellDireita.className !== 'S space') {
            cellDireita.appendChild(player)
            moveSong.playAudio()
        }
    }
    if (keyName == 'ArrowLeft' | keyName == 'a') {
        let cellEsquerda = document.querySelector(`div[id="${cellAtual - 1}"]`)
        if (cellEsquerda.className !== 'block' && cellEsquerda.className !== 'F space') {
            cellEsquerda.appendChild(player)
            moveSong.playAudio()
        }
    }
    checkWin()
}

function checkWin() {
    let player = document.querySelector('.player')
    let cell = player.parentElement.className
    if (cell == 'F space') {
        const winner = document.createElement('img')
        winner.id = 'winner'
        winner.src = './environment/youwin.png'
        main.appendChild(winner)
        winSong.playAudio()
        document.removeEventListener('keydown', movePlayer)
    }
}

function startScreen() {
    document.getElementById('start').addEventListener('keydown', function (event) {
        console.log('chamado')
        const key = event.key
        if (key == 'Enter') {
            setTimeout(() => { 
                const screen = document.getElementById('start')
                screen.style.display = 'none'
                screen.removeAttribute("tabindex")
                selectScreen()
                ThemeSong.playTheme()
            }, 200)
        }
    }, {once: true} )
}

function selectScreen() {


    const selectArea = document.getElementById('selectArea')
    window.selected = 'sonic'
    selectArea.addEventListener('click', selectPlayer)
    function selectPlayer(event) {
        console.log('selectPlayer() ', event)
        if ( event.target.id !== selected) {
            let clear = document.getElementById(selected)
            clear.className = ''
            
            let player = document.getElementById(event.target.id)
            player.className = 'selected'
            selected = event.target.id
        }
    }
    document.addEventListener('keydown', function(event) {
        if(event.key == 'Enter') {

            selectArea.removeEventListener('click', selectPlayer)
            makeLayrint(map)
            makePlayer(selected)
        
            document.getElementById('select').style.display = 'none'
            document.getElementById('players').style.display = 'none'
            document.addEventListener('keydown', movePlayer)


        }
    }, {once: true})
}



window.onload = () => {
    startScreen()
    
    
}

