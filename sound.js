class sound {
    constructor(src) {
        console.log('chamado 1')
        this.sound = document.createElement("audio")
        this.sound.src = src
        this.sound.setAttribute('preload', 'auto')
        this.sound.setAttribute('controls', 'none')
        this.sound.volume = 0.2
        this.sound.style.display = 'none'

        document.body.appendChild(this.sound)
        this.playAudio = function () {
            this.sound.volume = 0.1
            this.sound.play()
        }
        this.stopAudio = function () {
            this.sound.pause()
        }

        if (src == './sounds/theme.wav') {
            this.sound.id = 'themeSong'
            this.sound.loop = true
            this.sound.muted = true
        }
    } 

    playTheme() {
        document.addEventListener('keydown', function (event) {
            if (event.key == 'Enter') {
                ThemeSong.currentTime = 0
                ThemeSong.sound.muted = false
                ThemeSong.playAudio()
            }
            if (event.keyCode ==  32) {
                // press 'Space' for pause Theme song of game
                ThemeSong.stopAudio()

            }
        })

        document.getElementById('themeSong').addEventListener('timeupdate', function () {
            var buffer = .14
            if (this.currentTime > this.duration - buffer) {
                this.currentTime = 0
                this.play()
            }
        })
    }
}

const ThemeSong = new sound('./sounds/theme.wav')
const winSong = new sound('sounds/taDa.mp3')
const moveSong = new sound('./sounds/move.wav')
